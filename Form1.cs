﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Countdown
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public TimeSpan GetRemainingTime()
        {
            var dateTime1 = new DateTime(2014, 10, 16, 15, 0, 0);
            var dateTime2 = new DateTime(2014, 10, 17, 6, 30, 0);
            var dateTime3 = new DateTime(2014, 10, 17, 13, 0, 0);
            var dateTime4 = new DateTime(2014, 10, 20, 7, 0, 0);
            var dateTime5 = new DateTime(2014, 10, 20, 15, 0, 0);

            if (DateTime.Now < dateTime1)
            {
                return (dateTime1 - DateTime.Now).Add(TimeSpan.FromHours(14.5));
            }
            if (DateTime.Now < dateTime2)
            {
                return TimeSpan.FromHours(14.5);
            }
            if (DateTime.Now < dateTime3)
            {
                return (dateTime3 - DateTime.Now).Add(TimeSpan.FromHours(8.0));
            }
            if (DateTime.Now < dateTime4)
            {
                return TimeSpan.FromHours(8.0);
            }
            if (DateTime.Now < dateTime5)
            {
                return dateTime5 - DateTime.Now;
            }

            return TimeSpan.FromHours(0.0);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = string.Format("{0:hh}:{0:mm}:{0:ss}", GetRemainingTime());
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (label1.Width == 0)
            {
                return;
            }

            var font = label1.Font;
            for (var index = 1; index < int.MaxValue; ++index)
            {
                font = new Font(label1.Font.FontFamily, index, FontStyle.Bold);
                if (CreateGraphics().MeasureString(label1.Text, font).Width > (double)(label1.Width - 10))
                {
                    font = new Font(label1.Font.FontFamily, index - 1, FontStyle.Bold);
                    break;
                }
            }
            label1.Font = font;
        }
    }
}